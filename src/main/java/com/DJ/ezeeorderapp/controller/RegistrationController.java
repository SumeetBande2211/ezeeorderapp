package com.DJ.ezeeorderapp.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.util.JSONPObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.DJ.ezeeorderapp.dao.BusinessDao;
import com.DJ.ezeeorderapp.model.AddBusiness;
import com.DJ.ezeeorderapp.model.AppRegistration;
import com.DJ.ezeeorderapp.model.ChartData;
import com.DJ.ezeeorderapp.model.ProductMaster;
import com.DJ.ezeeorderapp.model.PutOrder;
import com.DJ.ezeeorderapp.model.RegisterToMyCT;
import com.DJ.ezeeorderapp.model.SubUser;
import com.DJ.ezeeorderapp.staticResponseStatus.StaticData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class RegistrationController{
	public static final Logger logger = Logger.getLogger(RegistrationController.class);
	StaticData staticData = new StaticData();
	@Autowired
	BusinessDao businessDao;
	private JdbcTemplate jdbcTemplate;
	ObjectMapper mapper = new ObjectMapper();
	String json = "";

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView register(@ModelAttribute("adduser") AppRegistration registration, BindingResult result) {
		return new ModelAndView("/registration");
	}

	@RequestMapping(value = "/verify", method = RequestMethod.GET)
	public ModelAndView otp(@ModelAttribute("adduser") AppRegistration appRegistration, ModelMap model,
			HttpSession session, BindingResult result) {

		return new ModelAndView("/otpverify");

	}

	@RequestMapping(value = "/ezeeorder/register", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> saveUser(@RequestBody AppRegistration appRegistration) throws Exception {
		ResponseEntity<String> responseEntity = null;

		appRegistration = businessDao.registerApp(appRegistration);
		responseEntity = new ResponseEntity<String>("OTP Sent successfully!", HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/ezeeorder/getalluser", method = RequestMethod.GET, produces = "application/json")
	public String getUser() {

		List<AppRegistration> list = businessDao.getAllUser();
		return "list";
	}

	@RequestMapping(value = "/ezeeorder/verifyotp", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> verifyotp(@RequestBody AppRegistration appRegistration) {
		ResponseEntity<String> responseEntity = null;
		if (businessDao.OTPVerify(appRegistration)) {
			responseEntity = new ResponseEntity<String>("OTP verified successfully!", HttpStatus.OK);
		} else {
			// businessDao.OTPVerify(appRegistration);
			responseEntity = new ResponseEntity<String>("OTP not verified !", HttpStatus.OK);
		}
		return responseEntity;
	}

	/*******************************************************************************/
	/**************
	 * App Registration for ezeeorder module
	 * 
	 * @throws Exception
	 *************/

	@RequestMapping(value = "/ezeeorder/subuser", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String subUser(@RequestBody SubUser subUser) throws JsonProcessingException {

		int obj = businessDao.subUserRegister(subUser);
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (obj == 1){
			String url = "https://www.auruminfo.com/Rest/AIwebservice/subuserCreation";
			   RestTemplate restTemplate = new RestTemplate();
			    HttpHeaders requestHeaders = new HttpHeaders(); 
			    List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
				acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
			    requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			    requestHeaders.setAccept(acceptableMediaTypes);
			    HttpEntity<?> subUser1 = new HttpEntity<>(subUser,requestHeaders);
			    ResponseEntity<SubUser> responseEntity =  restTemplate.exchange(url, HttpMethod.POST, subUser1, SubUser.class);
			    System.out.print("id : "+responseEntity.getBody());
			map.put("StatusCode", 200);
			map.put("response", subUser);
			map.put("message", "Registration Successful");
		}
		json = mapper.writeValueAsString(map);
		return json;

	}
	
	
	@RequestMapping(value="/ezeeorder/testservice", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	public void testService(@RequestBody SubUser data) throws IOException, JSONException {
		
		String url = "https://www.auruminfo.com/Rest/AIwebservice/subuserCreation";
		URL object = new URL(url);
		
		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestMethod("POST");
		
		JSONObject obj = new JSONObject();
		
		obj.put("user", "aispl2017");
		obj.put("password", "msb@0309");
		obj.put("address", "pune");
		obj.put("companyName", "aispl");
		obj.put("emailId", "aispl123@gmail.com");
		obj.put("firstName", "sumeet");
		obj.put("phoneNumber", "9326854760");
		obj.put("subuserName", "sumeet");
		obj.put("subuserPassword", "sumeet");
		System.out.println(obj);
//		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
//		wr.write(obj.toString());
//		wr.flush();
		
		StringBuffer buffer = new StringBuffer();
		int HttpResult = con.getResponseCode();
		System.out.println(HttpResult);
		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(
		            new InputStreamReader(con.getInputStream(), "UTF-8"));
		    String line = null;  
		    while ((line = br.readLine()) != null) {  
		        buffer.append(line + "\n");  
		    }
		    br.close();
		    System.out.println("" + buffer.toString()); 
		   
		}
		System.out.println("No response From server");
		
		
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		
//		TestData data1 = new TestData("100", "ABCTEST", "458735893749");
//		
//		RestTemplate restTemplate = new RestTemplate();
//		
//		TestData result = restTemplate.postForObject(url, data1, TestData.class);
//		
//		System.out.println(result);
	}
	
	@RequestMapping(value = "/ezeeorder/registerapp", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String registerApp(@RequestBody RegisterToMyCT register) throws JsonProcessingException {

		String obj = businessDao.register(register);
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (obj.equals("NewRegistration")) {
			map.put("StatusCode", 200);
			map.put("response", register);
			map.put("message", "Registration Successful");
		} else if (obj.equals("OTPNotVerified")) {
			map.put("StatusCode", 201);
			map.put("response", register);
			map.put("message", "Please verify OTP");
		} else if (obj.equals("OTPVerified")) {
			map.put("StatusCode", 203);
			map.put("response", register);
			map.put("message", "Already Registered");
		} else if (obj.equals("DeviceChanged")) {
			map.put("StatusCode", 202);
			map.put("response", register);
			map.put("message", "Device Changed,Please verify OTP");
		} else if (obj.equals("OTPNotVerifiedAndRoleChanged")) {
			map.put("StatusCode", 204);
			map.put("response", register);
			map.put("message", "RoleChangedAndOtpNotVerified");
		} else if (obj.equals("OTPVerifiedAndRoleChanged")) {
			map.put("StatusCode", 205);
			map.put("response", register);
			map.put("message", "OTPVerifiedAndRoleChanged");
		} else if (obj.equals("DeviceChangedWithRole")) {
			map.put("StatusCode", 206);
			map.put("response", register);
			map.put("message", "DeviceChangedWithRole");
		}

		json = mapper.writeValueAsString(map);
		return json;

	}


}
