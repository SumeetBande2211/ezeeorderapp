package com.DJ.ezeeorderapp.model;

public class AppRegistration {
	
	private Long userId;
	private String userName;
	private String userMobile;
	private String imei;
	private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	private String otp;
	
	
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	public String getOtp() {
		return otp; 
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "AppRegistration [userId=" + userId + ", userName=" + userName + ", userMobile=" + userMobile + ", imei="
				+ imei + ", status=" + status + ", otp=" + otp + "]";
	}
	
	
}
