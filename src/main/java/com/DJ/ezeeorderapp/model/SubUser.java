package com.DJ.ezeeorderapp.model;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName( "SubUser" )
public class SubUser implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int subUserId;
	private String address;
	private String user;
	private String password;
	private String companyName;
	private String emailId;
	private String firstName;
	private String phoneNumber;
	private String subuserName;
	private String subuserpassword;
	public int getSubUserId() {
		return subUserId;
	}
	public void setSubUserId(int subUserId) {
		this.subUserId = subUserId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSubuserName() {
		return subuserName;
	}
	public void setSubuserName(String subuserName) {
		this.subuserName = subuserName;
	}
	public String getSubuserpassword() {
		return subuserpassword;
	}
	public void setSubuserpassword(String subuserpassword) {
		this.subuserpassword = subuserpassword;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
