package com.DJ.ezeeorderapp.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

@Configuration
public class DBClass {
	@Bean(name="dataSource")
	
	public DriverManagerDataSource dataSource(){
		DriverManagerDataSource driverManagerDataSource =new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
		driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/ezeeorder");
		driverManagerDataSource.setUsername("root");
		driverManagerDataSource.setPassword("abhinav");
		return driverManagerDataSource;
	}
	
	/*@Bean(name="userDetailsService")
	public UserDetailsService userDetailsService(){
		JdbcDaoImpl daoImpl=new JdbcDaoImpl();
		daoImpl.setDataSource(dataSource());
		daoImpl.setUsersByUsernameQuery("select username,password,enabled from users where username=?");
		daoImpl.setAuthoritiesByUsernameQuery("select b.username, a.role from user_roles a, users b where b.username=? and a.userId=b.userId");
		return daoImpl;
	}*/

}
