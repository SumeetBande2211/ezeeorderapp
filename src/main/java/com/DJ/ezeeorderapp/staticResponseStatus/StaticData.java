package com.DJ.ezeeorderapp.staticResponseStatus;

public class StaticData {

	/******  NEW REGISTRATION STATUS CODE *****/
	
	public static final  Integer NEW_REGISTRATION=200;
	
	public static final  Integer REGISTRATION_SAME_OTP_NOT_VERIFIED=201;
	
	public static final  Integer NEW_IMEI=202;
	public static final  Integer REGISTRATION_SAME_OTP_VERIFIED=203;
	
	/******  NEW REGISTRATION RESPONSE MESSAGE *****/
	
	public static final String REGISTRATION_SUCESS="Registration Successful";
	public static final String ALREADY_REGISTER_VERIFY_OTP="Please verify OTP";
	public static final String DEVICE_CHANGED="Device Changed,Please verify OTP";
	public static final String ALREADY_REGISTER_OTP_VERIFIED="Already Registered";

	
	/********  OTP VERIFICATION STATUS CODE *********/
	
	public static final Integer OTP_VERIFIED=101;
	
	public static final Integer OTP_NOT_VERIFIED=102;
	
	/********   OTP VERIFICATION RESPONSE MESSAGE   *********/
	
	public static final String OTP_SUCCESS="OTP Verification Successful";
	
	public static final String OTP_FAILED="OTP Verification Failed";

	/**********  LOGIN VALIDATION STATUS CODE  **********/
	
	public static final Integer LOGIN_SUCCESS=301;
	public static final Integer LOGIN_FAILED=302;
	public static final Integer LOGIN_FROM_DIFFERENT_DEVICE=303;
	
	/**********  LOGIN VALIDATION RESPONSE MESSAGE   ***********/
	
	public static final String LOGIN_SUCCESSFUL="Login Successful";
	public static final String LOGIN_FAILED_MSG="Login Failed";
	public static final String LOGIN_FROM_DIFFERENT_DEVICE_MSG="Login From Different Device";
	
	/*********   FORGET PASSWORD STATUS CODE  **********/
	
	public static final Integer PASSWORD_EXIST=106;
	
	public static final Integer PASSWORD_NOT_EXIST=107;
	
	/*********   FORGET PASSWORD RESPONSE MESSAGE   **********/
	
	public static final String PASSWORD_RECIEVED="Password Sent to registered mobile";
	
	public static final String PASSWORD_NOT_RECIEVED="Mobile no not registered";

/*********  BUSINESS DATA UPLOAD STATUS CODE  **********/
	
	public static final Integer DATA_UPLOADED=108;
	
	public static final Integer DATA_NOT_UPLOADED=109;
	
	/*********   BUSINESS DATA UPLOADRESPONSE MESSAGE   **********/
	
	public static final String DATA_UPLOADED_MSG="Data Uploaded";
	
	public static final String DATA_NOT_UPLOADED_MSG="Data Not Uploaded";


/*********  BUSINESS DATA DOWNLOAD STATUS CODE  **********/
	
	public static final Integer DATA_DOWNLOADED=110;
	
	public static final Integer DATA_NOT_DOWNLOADED=111;
	
	/*********  BUSINESS DATA DOWNLOAD RESPONSE MESSAGE  **********/
	
	public static final String DATA_DOWNLOADED_MSG="Data Downloaded";
	
	public static final String DATA_NOT_DOWNLOADED_MSG="No Data Found";
	
	
	/******  OAUTH TOKEN ACCESS STATUS CODE  *******/

	public static final Integer UNAUTHORISED_ACCESS_CODE=400;
	
	
	/******  OAUTH TOKEN ACCESS RESPONSE MESSAGE  *******/
	
	public static final String UNAUTHORISED_ACCESS_MSG="Unauthorised Access!";
}



