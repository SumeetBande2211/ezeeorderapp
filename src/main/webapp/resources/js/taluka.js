function getTalukaForSelectedDistrict(){
	$('#loading').html('<img src="./resources/img/ajax-loader.gif">');
		var city = $('#city').val();
		$.ajax({
			url : 'registration/getTalukaForSelectedDistrict',
			method : 'post',
			ContentType : 'json',
			data : {
				city : city
			},

			success : function(response) {
				var options = '';
				if (response != null) {
					$(response).each(function(index, value) {
						options = options + '<option>' + value + '</option>';
					});
					$('#taluka').html(options);
				}
				$('#loading').html('');
			}
		});
	}