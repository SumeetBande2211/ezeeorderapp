package ezeeorderapp;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
//import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.DJ.ezeeorderapp.model.SubUser;

public class PostData {

	 public static void main(String args[]) {
//	        RestTemplate restTemplate = new RestTemplate();
//	        String url = "https://www.auruminfo.com/Rest/AIwebservice/subuserCreation";
//	        HttpHeaders headers = new HttpHeaders();
//	        headers.add("Content-Type", "application/json");
//	        headers.add("Accept", "application/json");
//	        
//	        Map<String, String> map = new HashMap<String, String>();
//	        map.put("id", "111");
//	        map.put("name", "Shyam");
//	        SubUser subUser = new SubUser();
//	        subUser.setPhoneNumber("9219209101");
//	        HttpEntity<String> entity1 = new HttpEntity<String>("Hello World!", headers);
//	        ResponseEntity<SubUser> entity= restTemplate.postForEntity(url, subUser, SubUser.class,entity1, map);
//	        System.out.println(entity.getBody().getPhoneNumber());
////	        System.out.println(entity.getBody().getAddress().getVillage());
		 
		 Map<String, String> vars = new HashMap<String, String>();
	        vars.put("id", "JS01");
	        
	        RestTemplate rt = new RestTemplate();
//            rt.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            String url = "https://www.auruminfo.com/Rest/AIwebservice/subuserCreation";
            SubUser subUser = new SubUser();
	        subUser.setPhoneNumber("9219209101");
	        subUser.setSubuserName("TEST DATA");
	        
	        SubUser returns = rt.postForObject(url, subUser, SubUser.class);
	        System.out.println(subUser.toString());
	    }
}
